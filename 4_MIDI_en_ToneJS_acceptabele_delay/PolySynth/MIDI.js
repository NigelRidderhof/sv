var webmidi;

//The rest of this file is easier to understand if you start reading the parts from the bottom and work your way up.

// This function passes on only the relevant MIDI inputs within a MIDI message for the right instrument.
function sendMIDIInput(name){	
	
	return function(message){
		
		data = message.data; // This gives us our [command/channel, note, velocity] data.
		console.log('MIDI data', data); // MIDI data [144, 63, 73]
		channel = data[0];
		note = data[1];
		velocity = data[2];
		
		switch (name) {
			case "wob":
				wobMIDIInput(velocity);
				break;
			case "wiggle":
				wiggleMIDIInput(note, velocity);
				break;
			case "drum":
				if (channel != 136 ){drumMIDIInput(note, velocity)};
				break;
			case "scan":
				ScanMIDIInput(channel, note);
				break;
			default:

		}

			// wob 
			// channel:		176
			// note:		12
			// velocity:	0-127

			// wiggle
			// channel: 	177
			// note:		z = 18			y = 17			x = 16
			// velocity:	0-127
			
			// drum 
			// channel:		152 & 136		(trigger start & trigger end)
			// note: 		down = min. 0, max. 125			left = min. 1, max 126			right = min. 2, max. 127
			// velocity:	1-127 & 127		(trigger start & trigger end)

			// scan 
			// channel:		145 & 129		(trigger start & trigger end)
			// note: 		0-127
			// velocity:	127
	}
};

function refresh() {

	var controllers = ["wob", "wiggle", "drum", "scan"];

	// If a controller is plugged in, this controller will get the status of "online" instead of "offline".
	if (webmidi.outputs.size) {
		
		webmidi.outputs.forEach(function(port) {
			
			function updateInstrumentsConnectionStatuses(port){
								
				controllers.forEach(controller => {
					var idString = controller + "ConnectionStatus";
					if (controller == port.name){
						document.getElementById(idString).innerHTML = "online";
					} else {
						document.getElementById(idString).innerHTML = "offline";
					}
				});
			}
			port.onmidimessage = updateInstrumentsConnectionStatuses(port);
		});

	} else {
		
		controllers.forEach(controller => {
			var idString = controller + "ConnectionStatus";
			document.getElementById(idString).innerHTML = "offline";
		});

	}

	// If MIDI input is detected, MIDI messages will be passed on to the function at the top.
	if (webmidi.inputs.size) {
		webmidi.inputs.forEach(function(port) {
			port.onmidimessage = sendMIDIInput(port.name);
		});
	} else {
		controllers.forEach(controller => {
			var idString = controller + "ConnectionStatus";
			document.getElementById(idString).innerHTML = "offline";
		});
	}

  }
  
  function fail(error) {
	var s = 'Cannot start WebMIDI';
	if (error) s += ': ' + error;
	alert(s);
  }
  
  function success(midiaccess) {
	webmidi = midiaccess;
	// When a MIDI controller get plugged in or out or creates input, the refresh function will be triggered.
	webmidi.onstatechange = refresh;
	refresh();
  }
  
  // Attempt to make WebMIDI connection
  try {
	navigator.requestMIDIAccess().then(success, fail);
  }
  catch (error) {
	alert("Cannot start WebMIDI: " + error);
  }