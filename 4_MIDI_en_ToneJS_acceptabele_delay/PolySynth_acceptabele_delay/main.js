class Instrument {
    constructor() {
        this.synth = new Tone.PolySynth(3, Tone.FMSynth);
        this.synth.toMaster();
    }

    togglePolySynth(channel, note){
        if (channel === 145) this.synth.triggerAttack(['C4', 'E4', 'G4'] 
        ,Tone.context.currentTime                                           // Method  1/3 
        );                                                                      
        else this.synth.releaseAll();
    }
}

const inst = new Instrument();


// A function for each instrument that processes the MIDI input. 

function wobMIDIInput(velocity){
    document.getElementById("logWobInput").innerHTML += "velocity: " + velocity + "<br>";
}

function wiggleMIDIInput(note, velocity){
    var inputs = "note: " + note + " | velocity: " + velocity;
    document.getElementById("logWiggleInput").innerHTML += inputs + "<br>";
}

function drumMIDIInput(note, velocity){
    var inputs = "note: " + note + " | velocity: " + velocity;
    document.getElementById("logDrumInput").innerHTML += inputs + "<br>";
}

function ScanMIDIInput(channel, note){
    var inputs = "channel: " + channel + " | note: " + note;
    document.getElementById("logScanInput").innerHTML += inputs + "<br>";

    inst.togglePolySynth(channel, note);
}
