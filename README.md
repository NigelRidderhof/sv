Welkom bij de online repository gebruikt tijdens mijn Open Innovation Stage bij Effenaar Smart Venue! 

Bij mijn stage houd ik me bezig met de exploratie van een interactieve en platformonafhankelijke tool die gebruik maakt van web-technologie, die het geven van een zekere cultuureducatie-workshop aan leerlingen toegankelijk maakt. Het doel bij deze workshop is de leerlingen leren over de mogelijkheden van de combinatie van technologie en muziek. Dit middels het laten ervaren van de mogelijkheden van de OWOW MIDI’s. 

Iedere map bevat één of meerdere POC’s m.b.t. dit onderdeel van de exploratie. Bij iedere POC zal een link naar een online demo van de POC bijgeleverd worden. Deze verschillende exploratie onderdelen worden geordend middels nummers op basis van de volgorde waarin ze gecreëerd zijn. Hetzelfde geldt voor de POC’s binnen de onderdelen, alleen wordt er dan middels letters geordend. 

Ik heb ervoor gekozen teksten zoals commit messages, variabel namen en comments die dus inhoudelijk te maken hebben met code in het Engels te schrijven, omdat deze taal over het algemeen meer gangbaar is binnen en ik gelievere hier ook mee wil oefenen. 

Voor vragen of feedback ben ik te bereiken via n.ridderhof@student.fontys.nl.

Nigel Ridderhof