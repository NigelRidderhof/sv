// A function for each instrument that processes the MIDI input. 

function wobMIDIInput(velocity){
    document.getElementById("logWobInput").innerHTML += "velocity: " + velocity + "<br>";
}

function wiggleMIDIInput(note, velocity){
    var inputs = "note: " + note + " | velocity: " + velocity;
    document.getElementById("logWiggleInput").innerHTML += inputs + "<br>";
}

function drumMIDIInput(note, velocity){
    var inputs = "note: " + note + " | velocity: " + velocity;
    document.getElementById("logDrumInput").innerHTML += inputs + "<br>";
}

function ScanMIDIInput(channel, note){
    var inputs = "channel: " + channel + " | note: " + note;
    document.getElementById("logScanInput").innerHTML += inputs + "<br>";
}