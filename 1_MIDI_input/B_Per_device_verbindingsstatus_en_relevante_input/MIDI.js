var midi, data, instrumentName;
// request MIDI access
if (navigator.requestMIDIAccess) {
	navigator.requestMIDIAccess({
		sysex: false
	}).then(onMIDISuccess, onMIDIFailure);
} else {
	alert("No MIDI support in your browser.");
}

// midi functions
function onMIDISuccess(midiAccess) {
	// when we get a succesful response, run this code
	midi = midiAccess; // this is our raw MIDI data, inputs, outputs, and sysex status

	var inputs = midi.inputs.values();
	// loop over all available inputs and listen for any MIDI input
	for (var input = inputs.next(); input && !input.done; input = inputs.next()) {
		// each time there is a midi message call the onMIDIMessage function
		input.value.onmidimessage = onMIDIMessage;
	}
	
	midi.outputs.forEach(function(port) {
		instrumentName = port.name;
	});

	var controllers = ["wob", "wiggle", "drum", "scan"];
	controllers.forEach(controller => {
		var idString = controller + "ConnectionStatus";
		if (controller == instrumentName){
			document.getElementById(idString).innerHTML = "online";
		} else {
			document.getElementById(idString).innerHTML = "offline";
		}
	});

}

function onMIDIFailure(error) {
	// when we get a failed response, run this code
	console.log("No access to MIDI devices or your browser doesn't support WebMIDI API. Please use WebMIDIAPIShim " + error);
}


function sendMIDIInput(instrumentName, channel, note, velocity){
	
	switch (instrumentName) {
		case "wob":
			wobMIDIInput(velocity);
			break;
		case "wiggle":
			wiggleMIDIInput(note, velocity);
			break;
		case "drum":
			if (channel != 136 ){drumMIDIInput(note, velocity)};
			break;
		case "scan":
			ScanMIDIInput(channel, note);
			break;
		default:

	}

	// if(velocity != 0 && data[0] !=128){
	// 	console.log("MIDI note:" + note);
	// }
};

function onMIDIMessage(message) {
	data = message.data; // this gives us our [command/channel, note, velocity] data.
	console.log('MIDI data', data); // MIDI data [144, 63, 73]
	channel = data[0];
	note = data[1];
	velocity = data[2];

	sendMIDIInput(instrumentName, channel, note, velocity);

	// wob 
	// channel:		176
	// note:		12
	// velocity:	0-127

	// wiggle
	// channel: 	177
	// note:		z = 18			y = 17			x = 16
	// velocity:	0-127
	
	// drum 
	// channel:		152 & 136		(trigger start & trigger end)
	// note: 		down = 48		left = 49		right = 50
	// velocity:	1-127 & 127		(trigger start & trigger end)

	// scan 
	// channel:		145 & 129		(trigger start & trigger end)
	// note: 		0-127
	// velocity:	127

}
